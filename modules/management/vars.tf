# SPDX-License-Identifier: GPL-3.0-or-later

variable "resource_group_name" {
  type = string
}

variable "location" {
  type = string
}

variable "network" {
}

variable "ssh_key" {
  type = string
}

variable "vm_custom_data" {
  type = string
}

variable "vm_size_override" {
  type    = string
  default = ""
}

variable "disk_size_monitor" {
  type    = number
  default = 10
}

variable "set_domain_name_label" {
  type    = bool
  default = false
}
