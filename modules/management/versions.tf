# SPDX-License-Identifier: GPL-3.0-or-later

terraform {
  required_version = ">= 0.12"
}
