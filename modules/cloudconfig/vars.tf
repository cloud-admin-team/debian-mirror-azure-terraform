# SPDX-License-Identifier: GPL-3.0-or-later

variable "users" {
  type = list(string)
}
